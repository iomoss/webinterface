WebPrinter
========

The goal of this project is to provide a minimal 3D printer webserver interface with Angular2.0 and Typescript.

## Install

Clone this repo and execute in your favourite shell:

* `npm i -g typescript webpack webpack-dev-server` to globally install typescript and webpack
* `npm i` to install local npm dependencies
* `tsd link` to generate typescript includes

## Development

After completing installation type in your favourite shell:

* `npm start` to compile the entire project, and bootup the webpack-dev-server on port 3000.

Note: Source files are being watched and will be re-transpiled on each change.
Note: The webpack-dev-server can be reached on  [`http://localhost:3000/`](http://localhost:3000)

## Release

To build a release image run;

* `npm run deploy` to compile, minimize and pack the project. Output files will be generated in `./build`

These files can be hosted on any webserver.
