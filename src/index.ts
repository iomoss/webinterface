/// <reference path="Directives/util/tabs.ts" />
/// <reference path="Directives/util/collapse.ts" />
/// <reference path="Directives/jog/JogPanel.ts" />

import {Component, View, bootstrap} from 'angular2/angular2';
import {HTTP_BINDINGS} from 'angular2/http';

import {JogPanel} from './Directives/jog/JogPanel';

import {TabTag} from './Directives/util/tabs';
import {CollapseTag} from './Directives/util/collapse';

// Annotation section
@Component({
    selector: 'app'
})
@View({
    template: `
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <div class="navbar-header">
              <a class="navbar-brand" href="#">
                  <i class="fa fa-asterisk"></i>
                  IOMOSS
              </a>
            </div>
          </div>
        </nav>
        <!-- Image for reference -->
        <!--
        <img src="images/header.png" width="100%"></img>
        -->

        <div>
            <div class="col-md-4">
            <collapse>
                <div button style="width:100%">
                    <button type="button" class="btn btn-default btn-group-justified text-left" style="text-align: left;">
                    <div>
                        <i class="fa fa-signal fa-fw"></i>
                        Connection
                    </div>
                    </button>
                </div>
                <b>Serial Port:</b><br/>

                <div dropdown>
                    <button type="button" class="btn btn-default btn-group-justified" style="text-align: left;" dropdown-toggle>
                      /dev/ttyACM0
                      <span class="caret"></span>
                    </button>
                </div>
                <br/>

                <b>Baudrate:</b><br/>
                <div dropdown>
                    <button type="button" class="btn btn-default btn-group-justified" style="text-align: left;" dropdown-toggle>
                      115200
                      <span class="caret"></span>
                    </button>
                </div>
                <br/>

                <input type="checkbox"></input>
                    Checkbox Save connection settings
                <br/>
                <input type="checkbox"></input>
                    Auto-connect on server startup
                <br/>

                <button type="button" class="btn btn-default btn-group-justified text-left">
                    <!-- TODO: Connect or disconnect based on current state -->
                    Disconnect
                </button>

                <!-- Image for reference -->
                <!--
                <img src="images/connection.png"></img>
                -->
            </collapse>
            <collapse>
                <div button style="width:100%">
                    <button type="button" class="btn btn-default btn-group-justified" style="text-align: left;">
                        <i class="fa fa-info-circle fa-lg"></i>
                        State
                    </button>
                </div>
                Machine State: <b>Operational</b><br/>
                File:<br/>
                Filament:<br/>
                Estimated Print Time:<br/>
                Height: <b>-</b><br/>
                Print Time:<br/>
                Print Time Left:<br/>
                Printed: <b>-</b><br/>
                <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                </div>

                <div class="btn-group btn-group-justified" role="group">

                <div class="btn-group" role="group">
                <button type="button" class="btn btn-primary">
                    <i class="fa fa-print fa-lg"></i>
                    Print
                </button>
                </div>

                <div class="btn-group" role="group">
                <button type="button" class="btn btn-default">
                    <i class="fa fa-pause fa-lg"></i>
                    Pause
                </button>
                </div>

                <div class="btn-group" role="group">
                <button type="button" class="btn btn-default">
                    <i class="fa fa-stop fa-lg"></i>
                    Cancel
                </button>
                </div>
                </div>

                <!-- Image for reference -->
                <!--
                <img src="images/state.png"></img>
                -->
            </collapse>
            <collapse>
                <div button style="width:100%">
                    <button type="button" class="btn btn-default btn-group-justified" style="text-align: left;">
                        <i class="fa fa-list fa-lg"></i>
                        Files
                    </button>
                </div>
                <div style="width:100%">
                <div class="col-md-6">
                <b>Name</b>
                </div>
                <div class="col-md-2">
                <b>Size</b>
                </div>
                <div class="col-md-4" style="text-align: center">
                <b>Action</b>
                </div>
                </div>

                <!-- TODO: Duplicate this 'x' times -->
                <div style="width:100%">
                <div class="col-md-6">
                Filename_here.gcode
                </div>
                <div class="col-md-2">
                6.7MB
                </div>
                <div class="col-md-4" style="text-align: center">
                <i class="fa fa-trash"></i> |
                <i class="fa fa-folder-open"></i> |
                <i class="fa fa-print"></i>
                </div>
                </div>

                <div style="width:100%;text-align: right">
                    Free: 5.2GB
                </div>

                <nav>
                  <ul class="pagination">
                    <li>
                      <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                      </a>
                    </li>
                    <li><a href="#">1</a></li>
                    <li>
                      <a href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                      </a>
                    </li>
                  </ul>
                </nav>

                <div class="btn-group btn-group-justified" role="group">

                <div class="btn-group" role="group">
                <button type="button" class="btn btn-primary">
                    <i class="fa fa-upload fa-lg"></i>
                    Upload
                </button>
                </div>

                <div class="btn-group" role="group">
                <button type="button" class="btn btn-primary">
                    <i class="fa fa-upload fa-lg"></i>
                    Upload to SD
                </button>
                </div>
                </div>

                <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                Hint: You can also drag and drop files on this page to upload them.

                <!-- Image for reference -->
                <!--
                <img src="images/files.png"></img>
                -->
            </collapse>
            </div>

            <div class="col-md-8">
                <JogPanel></JogPanel>
                <!-- Image for reference -->
                <img src="images/main_window.png"></img>
            </div>
        </div>
    `,
    directives: [TabTag, JogPanel, CollapseTag]
})
// Component controller
class MyAppComponent { }

bootstrap(MyAppComponent, [HTTP_BINDINGS]);
