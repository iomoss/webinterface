/// <reference path="./XY.ts" />
/// <reference path="./Z.ts" />
/// <reference path="./E.ts" />
/// <reference path="./Length.ts" />
/// <reference path="./Additional.ts" />

let html = require('./JogPanel.html');

import {
  Component,
  View,
  NgFor,
  Host
  } from 'angular2/angular2';

  import {XY} from './XY';
  import {Z} from './Z';
  import {E} from './E';
  import {Length} from './Length';
  import {Additional} from './Additional';

@Component({
    selector: 'JogPanel'
})
@View({
    template: html,
    directives: [XY, Z, E, Length, Additional]
})
export class JogPanel { }
