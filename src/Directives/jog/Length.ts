import {
  Component,
  View,
  NgModel
  } from 'angular2/angular2';

import {ButtonRadio} from 'ng2-bootstrap';

let html = require('./Length.html');
let css  = require('./no_padding.css');

@Component({
    selector: 'Length'
})
@View({
    template: html,
    styles: [css],
    directives: [ButtonRadio, NgModel]
})
export class Length
{
  private radioModel:string = '1';

  valueChange(event) : void
  {
      // TODO: Connect to glue layer
      alert(event.target.innerText)
  }
}
