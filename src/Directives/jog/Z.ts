import {
  Component,
  View,
  NgFor,
  Host
  } from 'angular2/angular2';

let html = require('./Z.html');
let css  = require('./no_padding.css');

@Component({
    selector: 'Z'
})
@View({
    template: html,
    styles: [css]
})
export class Z {
    constructor() {
    }
}
