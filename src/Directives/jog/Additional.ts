import {
  Component,
  View,
  NgFor,
  Host
  } from 'angular2/angular2';

let html = require('./Additional.html');
let css  = require('./no_padding.css');

@Component({
    selector: 'Additional'
})
@View({
    template: html,
    styles: [css]
})
export class Additional {
    constructor() {
    }
}
