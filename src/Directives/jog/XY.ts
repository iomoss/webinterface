import {
  Component,
  View,
  NgFor,
  Host
  } from 'angular2/angular2';

let html = require('./XY.html');
let css  = require('./no_padding.css');

@Component({
    selector: 'XY'
})
@View({
    template: html,
    styles: [css]
})
export class XY {
    constructor() {
    }
}
