import {
  Component,
  View,
  NgFor,
  Host
  } from 'angular2/angular2';

let html = require('./E.html');
let css  = require('./no_padding.css');

@Component({
    selector: 'E'
})
@View({
    template: html,
    styles: [css]
})
export class E {
    constructor() {
    }
}
