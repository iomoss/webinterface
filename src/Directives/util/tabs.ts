/// <reference path="../../../typings/tsd.d.ts" />
import {Component, View, Host, NgFor, ElementRef} from 'angular2/angular2';

import {tabs} from 'ng2-bootstrap';

let html = require('./tabs.html');

@Component({
  selector: 'tabs'
})
@View({
  template: html,
  directives: [tabs, NgFor]
})
class TabContainer
{
    private tabs:Array<Tab>;

    constructor()
    {
        this.tabs = [];
    }

    selectTab(tab: Tab) 
    {
        this.tabs.forEach((tab) => 
        {
            tab.active = false;
        });
        tab.active = true;
    }

    addTab(tab: Tab) 
    {
        if (this.tabs.length === 0) 
        {
            tab.active = true;
        }
        this.tabs.push(tab);
    }
}

@Component({
  selector: 'tab'
})
@View({
  template: ``
})
class Tab
{
    active : boolean;
    title : string;
    content : string;
    disabled : boolean;

    // TODO: Pull out the content somehow
    constructor(elementRef : ElementRef, @Host() tabs:TabContainer) 
    {
        this.title = "No-title-given";
        this.content = "<b>No-content-found</b>";
        this.active = false;
        this.disabled = false;
        tabs.addTab(this);
    }
}

export const TabTag:Array<any> = [TabContainer, Tab];
