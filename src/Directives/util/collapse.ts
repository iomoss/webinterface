import {
  Component,
  View,
  Host
  } from 'angular2/angular2';

let html = require('./collapse.html');

import {Collapse} from 'ng2-bootstrap';

@Component({
  selector: 'collapse',
})
@View({
  template: html,
  directives: [Collapse]
})
export class CollapseTag
{
    public isCollapsed:boolean = false;

    constructor() 
    {
    }
}
